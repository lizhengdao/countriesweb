## Table of contents
* [Demo](#demo)
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

# [Click here to view live demo](https://react-countries-web.herokuapp.com/)

## General info
Simple search and display of information about countries using React.    
Data fetched from [REST Countries](https://restcountries.eu)
	
## Technologies
Project was created with:
* [React](https://reactjs.org)
* [tailwindcss](https://tailwindcss.com)
	
## Setup
To run this project, install it locally using npm:

```
$ npm i
$ npm start
```