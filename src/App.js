import React, { useState, useEffect } from "react";
import "./styles/App.css";
// Components
import Search from "./components/Search";
import useFetchData from "./utils/useFetchData";
import Card from "./components/Card/Card";
import CardCountry from "./components/CardCountry/CardCountry";
import Results from "./components/Results";

function App() {
  const [searchData, setSearchData] = useState([]);
  const [country, setCountry] = useState(null);
  const [searchValue, setSearchValue] = useState("");
  const [resultsCount, setResultsCount] = useState(0);
  // Custom hook
  const { data, isLoading, error } = useFetchData();

  useEffect(() => {
    // If loading is finished show data
    if (!isLoading) {
      setSearchData(data);
      setResultsCount(data.length);
    }
  }, [data, isLoading]);

  const handleSearch = e => {
    // If country was shown remove it
    const value = e.target.value;
    setSearchValue(value);

    if (country) {
      setCountry(null);
    }
    // If search is not empty
    if (value.length) {
      const filterResults = data.filter(country =>
        country.name.toLowerCase().includes(value.toLowerCase())
      );
      setSearchData(filterResults);
      setResultsCount(filterResults.length);
    } else {
      setSearchData(data);
      setResultsCount(data.length);
    }
  };

  const handleShowCountry = e => {
    e.preventDefault();
    const code = e.currentTarget.value;
    const country = searchData.find(country => country.numericCode === code);

    setCountry(country);
    setSearchData([]);
    setSearchValue(country.name);
    setResultsCount(1);
  };

  const displayContent = () => {
    if (isLoading) {
      return <div className="spinner"></div>;
    } else if (!!!isLoading && !!!error) {
      return searchData.map(country => {
        return (
          <Card
            key={country.numericCode}
            country={country}
            handleShowCountry={handleShowCountry}
          />
        );
      });
    } else if (error) {
      return <p>Something went wrong with API....</p>;
    }
  };

  return (
    <div className="App min-h-screen max-w-screen bg-teal-500">
      <div className="flex flex-col items-center lg:pt-56 md:pt-56 sm:pt-56 xs:pt-10">
        <Results count={resultsCount} />
        <Search handleSearch={handleSearch} value={searchValue} />
      </div>
      <div className="flex flex-wrap justify-center mt-32 lg:mt-32 md:mt-10 sm:mt-32 xs:mt-8 p-8">
        {country ? <CardCountry country={country} /> : displayContent()}
      </div>
    </div>
  );
}

export default App;
