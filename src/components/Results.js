import React from "react";

export default function Results({ count }) {
  const result = count === 1 ? "country" : "countries";

  return (
    <p className="text-yellow-500 text-base xs:text-3xl sm:text-3xl md:text-4xl lg:text-4xl mb-6 font-raleway font-bold">
      Found {count + " " + result}
    </p>
  );
}
