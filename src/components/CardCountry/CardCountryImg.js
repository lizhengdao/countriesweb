import React from "react";

export default function CardCountryImg({ flag }) {
  return (
    <div className="w-64 md:w-auto lg:w-64 h-full sm:w-auto">
      <img className="object-cover" src={flag} alt="Country flag" />
    </div>
  );
}
