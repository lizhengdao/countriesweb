import React from "react";

export default function CardTitle({ name }) {
  return (
    <div className="p-2">
      <p className="font-bold text-blue-900">{name}</p>
    </div>
  );
}
