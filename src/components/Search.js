import React from "react";

export default function Search({ handleSearch, value }) {
  return (
    <input
      className="text-base sm:text-sm md:text-base lg:text-base outline-none text-gray-700 xs:w-8/12 sm:w-6/12 md:w-4/12 lg:w-4/12 h-12 p-2 pb-0 border-yellow-500 border-b-4 font-raleway shadow-md"
      type="search"
      placeholder="Type a country name here...."
      onChange={handleSearch}
      value={value}
    />
  );
}
